var gSelectedMenuStructure = {
    kichCo: "",
    duongKinh: "",
    suon: "",
    salad: "",
    coca: "",
    thanhTien: "",
}
var gUserOrder = {
    fullName: "",
    email: "",
    phone: "",
    address: "",
    maVoucher: "",
    loiNhan: "",
    maNuocUong: "",
}
const gObjectRequest = {
    fullName: "",
    email: "",
    phone: "",
    address: "",
    kichCo: "",
    duongKinh: "",
    suon: "",
    salad: "",
    loaiPizza: "",
    maVoucher: "",
    giamGia: "",
    thanhTien: "",
    maNuocUong: "",
    soLuongNuoc: "",
    loiNhan: "",
    trangThai: "opened"
}
var gPizzaTypeSelected = {
    pizzaType: "",
}
var gUserOrderData = [];
var gPriceResult = {
    price: "",
    discount: "",
}
$(document).ready(function () {
    // khu vực gọi hàm xử lý nút
    // khi bấm nút chọn pizza 
    $("#btn-SPizza").on('click', function () {
        onBtnSizeSClicks();
    });
    $("#btn-MPizza").on('click', function () {
        onBtnSizeMClicks();
    });
    $("#btn-LPizza").on('click', function () {
        onBtnSizeLClicks();
    });
    $("#btn-hawaii").on('click', function () {
        onBtnClickPickHawaii();
    });
    $("#btn-haisan").on('click', function () {
        onBtnClickPickHaisan();
    });
    $("#btn-hunkhoi").on('click', function () {
        onBtnClickPickHunkhoi();
    });
    $("#sendOrder-btn").on('click', function () {
        onBtnClickSendOrder();
    })
    onPageLoading();
    $("#btn-create-order").on('click', function () {
        onClickCreateOrder();
    });
});
// function onPageLoading() 
function onPageLoading() {
    // hàm gọi api
    callApiGetDrinkingList();

}
// function gửi đơn hàng
function onBtnClickSendOrder() {
    readOrderInformation(gUserOrder);
    var vCheckUserOrder = validateUserOrder(gUserOrder);
    if (vCheckUserOrder) {
        $("#detail-order").modal('show');
        loadUserDataToModel(gUserOrder);
        if (gUserOrder.maVoucher != "") {
            $.ajax({
                url: "/voucherCode/?maVoucher=" + gUserOrder.maVoucher,
                type: "GET",
                dataType: "json",
                success: function (responseJson) {
                    console.log(responseJson);
                    if (responseJson.data === null) {
                        alert("Mã voucher nhập sai!");
                        confirmOrderUser(responseJson);
                    } else {
                        loadDiscount(responseJson);
                        gUserOrderData = responseJson.data;
                        confirmOrderUserVoucher(responseJson);
                    }
                },
                error: function (responseJson) {
                    alert("Mã voucher nhập sai!");
                    confirmOrderUser(responseJson);
                }
            });
        }
        else {
            alert("voucher không tồn tại")
            confirmOrderUser(gUserOrderData);
        }
    }
}
// function tạo order 
function onClickCreateOrder() {
    getInforOrderCreate(gObjectRequest);
    console.log(gObjectRequest)
    $.ajax({
        // url: "/devcamp-pizza365/orders?email=" + gUserOrder.email,
        url: "/devcamp-pizza365/orders" ,
        type: "POST",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify(gObjectRequest),
        success: function (responseJson) {
            console.log(responseJson);
            showConfirmSign(responseJson);
        },
        error: function (responseJson) {
        }
    });
}
// function chọn loại menu size s
function onBtnSizeSClicks() {
    var vSelectedMenuStructure = { kichCo: "S", duongKinh: "20", suon: "2", salad: "200", coca: "2", thanhTien: "150000", }
    gSelectedMenuStructure = vSelectedMenuStructure;
    changeColorCombo();
}
// function chọn loại menu size m
function onBtnSizeMClicks() {
    var vSelectedMenuStructure = { kichCo: "M", duongKinh: "25", suon: "4", salad: "300", coca: "3", thanhTien: "200000", }
    gSelectedMenuStructure = vSelectedMenuStructure;
    changeColorCombo();
}
// function chọn loại menu size l
function onBtnSizeLClicks() {
    var vSelectedMenuStructure = { kichCo: "L", duongKinh: "30", suon: "8", salad: "500", coca: "4", thanhTien: "250000", }
    gSelectedMenuStructure = vSelectedMenuStructure;
    changeColorCombo();
}
function changeColorCombo() {
    if (gSelectedMenuStructure.kichCo === "S") {
        $("#btn-SPizza").removeClass().addClass("btn btn-success w-100");
        $("#btn-MPizza").removeClass().addClass("btn btn-warning w-100");
        $("#btn-LPizza").removeClass().addClass("btn btn-warning w-100");
    }
    if (gSelectedMenuStructure.kichCo === "M") {
        $("#btn-SPizza").removeClass().addClass("btn btn-warning w-100");
        $("#btn-MPizza").removeClass().addClass("btn btn-success w-100");
        $("#btn-LPizza").removeClass().addClass("btn btn-warning w-100");
    }
    if (gSelectedMenuStructure.kichCo === "L") {
        $("#btn-SPizza").removeClass().addClass("btn btn-warning w-100");
        $("#btn-MPizza").removeClass().addClass("btn btn-warning w-100");
        $("#btn-LPizza").removeClass().addClass("btn btn-success w-100");
    }
    // hiển thị kết quả chọn combo
    console.log("%c Menu được chọn là: ", "color:blue");
    console.log("Loại Combo: " + gSelectedMenuStructure.kichCo);
    console.log("Kích thước: " + gSelectedMenuStructure.duongKinh);
    console.log("Sườn nướng: " + gSelectedMenuStructure.suon);
    console.log("Salad: " + gSelectedMenuStructure.salad);
    console.log("CocaCola: " + gSelectedMenuStructure.coca + "chai");
    console.log("Thành tiền: " + gSelectedMenuStructure.thanhTien + "VND");
}
// bấm nút các loại pizza
function onBtnClickPickHawaii() {
    var vPizza = handlePizzaType("OCEAN MANIA");
    gPizzaTypeSelected.pizzaType = vPizza.pizzaName;
    changeColorPizza();
}
function onBtnClickPickHaisan() {
    var vPizza = handlePizzaType("HAWAIIAN");
    gPizzaTypeSelected.pizzaType = vPizza.pizzaName;
    changeColorPizza();
}
function onBtnClickPickHunkhoi() {
    var vPizza = handlePizzaType("CHEESY CHICKEN BACON");
    gPizzaTypeSelected.pizzaType = vPizza.pizzaName;
    changeColorPizza();
}
// function lấy thông tin loại pizza được chọn
function handlePizzaType(paramType) {
    var vPizza = {
        pizzaName: paramType,
    }
    return vPizza;
}
//function đổi màu loại pizza
function changeColorPizza() {
    if (gPizzaTypeSelected.pizzaType === "OCEAN MANIA") {
        $("#btn-hawaii").removeClass().addClass("btn btn-success w-100");
        $("#btn-haisan").removeClass().addClass("btn btn-warning w-100");
        $("#btn-hunkhoi").removeClass().addClass("btn btn-warning w-100");
    }
    if (gPizzaTypeSelected.pizzaType === "HAWAIIAN") {
        $("#btn-hawaii").removeClass().addClass("btn btn-warning w-100");
        $("#btn-haisan").removeClass().addClass("btn btn-success w-100");
        $("#btn-hunkhoi").removeClass().addClass("btn btn-warning w-100");
    }
    if (gPizzaTypeSelected.pizzaType === "CHEESY CHICKEN BACON") {
        $("#btn-hawaii").removeClass().addClass("btn btn-warning w-100");
        $("#btn-haisan").removeClass().addClass("btn btn-warning w-100");
        $("#btn-hunkhoi").removeClass().addClass("btn btn-success w-100");
    }
    console.log("%c Loại pizza được chọn là: ", "color: blue");
    console.log("Pizza được chọn: " + gPizzaTypeSelected.pizzaType);
}
// function lấy thông tin user order
function readOrderInformation(paramUserOrder) {
    paramUserOrder.fullName = $("#inp-fullname").val().trim();
    paramUserOrder.email = $("#inp-email").val().trim();
    paramUserOrder.phone = $("#inp-dien-thoai").val().trim();
    paramUserOrder.address = $("#inp-dia-chi").val().trim();
    paramUserOrder.maVoucher = $("#inp-ma-giam-gia").val().trim();
    paramUserOrder.loiNhan = $("#inp-loi-nhan").val().trim();
    paramUserOrder.maNuocUong = $("#chondouong-inp option:selected").val();
}
//function validate user order
function validateUserOrder(paramUserOrder) {
    if (paramUserOrder.fullName === "") {
        alert("Bạn chưa nhập tên, vui lòng nhập vào!");
        return false;
    }
    if (!emailValidate(paramUserOrder.email)) {
        alert("Bạn chưa nhập email hoặc nhập sai định dạng!");
        return false;
    }
    if (paramUserOrder.phone === "") {
        alert("Bạn chưa nhập số điện thoại, vui lòng nhập vào!");
        return false;
    }
    if (paramUserOrder.address === "") {
        alert("Bạn chưa nhập địa chỉ, vui lòng nhập vào!");
        return false;
    }
    if (gSelectedMenuStructure.kichCo === "") {
        console.log(gSelectedMenuStructure.kichCo);
        alert("Chưa chọn loại combo!")
        window.location.href = "43.html#Menus";
        return false;
    }
    if (gPizzaTypeSelected.pizzaType === "") {
        console.log("Chưa chọn loại pizza");
        alert("chưa chọn loại pizza")
        window.location.href = "43.html#about";
        return false;
    }
    if (gUserOrder.maNuocUong === "none") {
        console.log("Chưa chọn loại nước uống");
        alert("chưa chọn nước uống")
        window.location.href = "43.html#drinkk";
        return false;
    }
    return true;
}

// function get infor create order
function getInforOrderCreate(paramObjectRequest) {
    paramObjectRequest.kichCo = gSelectedMenuStructure.kichCo;
    paramObjectRequest.duongKinh = gSelectedMenuStructure.duongKinh;
    paramObjectRequest.suon = gSelectedMenuStructure.suon;
    paramObjectRequest.salad = gSelectedMenuStructure.salad;
    paramObjectRequest.loaiPizza = gPizzaTypeSelected.pizzaType;
    paramObjectRequest.maVoucher = gUserOrder.maVoucher;
    paramObjectRequest.maNuocUong = gUserOrder.maNuocUong;
    paramObjectRequest.soLuongNuoc = gSelectedMenuStructure.coca;
    paramObjectRequest.fullName = gUserOrder.fullName;
    paramObjectRequest.thanhTien = gSelectedMenuStructure.thanhTien;
    paramObjectRequest.email = gUserOrder.email;
    paramObjectRequest.phone = gUserOrder.phone;
    paramObjectRequest.address = gUserOrder.address;
    paramObjectRequest.loiNhan = $("#inp-loi-nhan").val().trim();
    paramObjectRequest.giamGia = gPriceResult.discount;
}
// function hiển thị cảm ơn
function showConfirmSign(paramConfirm) {
    $("#confirm-order").modal('show');
    $("#confirn-inp").val(paramConfirm.data.orderCode);
}
// function hiển thị thông tin lên modal
function loadUserDataToModel(paramUserOrder) {
    console.log(paramUserOrder)
    $("#inp-hoten-modal").val(paramUserOrder.fullName);
    $("#inp-email-modal").val(paramUserOrder.email);
    $("#inp-sdt-modal").val(paramUserOrder.phone);
    $("#inp-diachi-modal").val(paramUserOrder.address);
    $("#inp-loinhan-modal").val(paramUserOrder.loiNhan);
    $("#inp-voucherid-modal").val(paramUserOrder.maVoucher);
}
function confirmOrderUserVoucher() {
    $("#inp-detail-modal").html("Xác nhận: " + "Tên: " + gUserOrder.fullName + ", " + "Số điện thoại: " + gUserOrder.phone + ", địa chỉ:" + gUserOrder.address + "," + "&#10;" +
        "Menu: " + gSelectedMenuStructure.kichCo + ", Sườn nướng: " + gSelectedMenuStructure.suon + ", Nước: " + gUserOrder.maNuocUong + ", Salad: " + gSelectedMenuStructure.salad + ", Drink: " + gSelectedMenuStructure.coca + ", " + "&#10;" +
        "Loại pizza: " + gPizzaTypeSelected.pizzaType + ", Thành tiền: " + gSelectedMenuStructure.thanhTien + "VND, " + "Mã giảm giá: " + gUserOrder.maVoucher + ", " + "&#10;" +
        "Phải thanh toán: " + gPriceResult.price + "(Giảm giá " + gPriceResult.discount + "%)");
}
function confirmOrderUser() {
    $("#inp-detail-modal").html("Xác nhận: " + "Tên: " + gUserOrder.fullName + ", " + "Số điện thoại: " + gUserOrder.phone + ", địa chỉ:" + gUserOrder.address + "," + "&#10;" +
        "Menu: " + gSelectedMenuStructure.kichCo + ", Sườn nướng: " + gSelectedMenuStructure.suon + ", Nước: " + gUserOrder.drink + ", Salad: " + gSelectedMenuStructure.salad + ", Drink: " + gSelectedMenuStructure.coca + ", " + "&#10;" +
        "Loại pizza: " + gPizzaTypeSelected.pizzaType + ", Thành tiền: " + gSelectedMenuStructure.thanhTien + "VND, " + "Mã giảm giá: " + gUserOrder.maVoucher + ", " + "&#10;" +
        "Phải thanh toán: " + gSelectedMenuStructure.thanhTien + "(Giảm giá " + "0" + "%)");
}
// tinhs giảm giả
function loadDiscount(paramDiscount) {
    var vPrice = gSelectedMenuStructure.thanhTien;
    var vPriceResult = vPrice - ((vPrice * paramDiscount.data.phanTramGiamGia) * 0.01);
    gPriceResult.discount = paramDiscount.data.phanTramGiamGia;
    gPriceResult.price = vPriceResult;
    console.log(paramDiscount)
    console.log(gPriceResult.discount)
}
// function lấy drink list
function callApiGetDrinkingList() {
    $.ajax({
        // url: "/drink",
        url: "/devcamp-pizza365/drink",
        type: "GET",
        dataType: "json",
        success: function (responseJson) {
            // load drink vào select
            console.log(responseJson)
            loadDrinkIntoSelection(responseJson.data);
        },
        error: function (ajaxContext) {
            alert(ajaxContext.responseText);
        }
    });
}
// function load đồ uống vào select
function loadDrinkIntoSelection(paramResponseJson) {
    for (var i = 0; i < paramResponseJson.length; i++) {
        $("#chondouong-inp").append($("<option>", {
            text: paramResponseJson[i].tenNuocUong,
            value: paramResponseJson[i].maNuocUong
        }));
    }
}
function emailValidate(paramEmail) {
    return !!/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(paramEmail);
}