var gID = [];
var gAllOrderData = [];
var gORDERID = [];
var gVoucherID = [];
var gThanhTien = [];
var gPizzaSize = ["S", "M", "L"];
var gPizzaType = ["HAWAII", "SEAFOOD", "BACON"];
const gSelectedMenuStructure = [
    {
        menuName: "s",
        duongKinhCM: 20,
        suongNuong: 2,
        saladGr: 200,
        drink: 2,
        priceVND: 150000,
    },
    {
        menuName: "m",
        duongKinhCM: 25,
        suongNuong: 4,
        saladGr: 300,
        drink: 3,
        priceVND: 200000,
    },
    {
        menuName: "l",
        duongKinhCM: 30,
        suongNuong: 8,
        saladGr: 500,
        drink: 4,
        priceVND: 250000,
    },
]
"use strict";
$(document).ready(function () {

    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    const gCOLUMN_ID = 0;
    const gCOLUMN_SIZE = 1;
    const gCOLUMN_TYPE = 2;
    const gCOLUMN_DRINK = 3;
    const gCOLUMN_PRICE = 4;
    const gCOLUMN_NAME = 5;
    const gCOLUMN_PHONE = 6;
    const gCOLUMN_STATUS = 7;
    const gCOLUMN_ACTION = 8;
    const gUSER_COLS = ["orderCode", "kichCo", "loaiPizza", "maNuocUong", "thanhTien", "hoTen", "soDienThoai", "trangThai", "action"];
    // định nghĩa table  - chưa có data
    var gOrderTable = $("#order-table").DataTable({
        // Khai báo các cột của datatable
        "columns": [
            { "data": gUSER_COLS[gCOLUMN_ID] },
            { "data": gUSER_COLS[gCOLUMN_SIZE] },
            { "data": gUSER_COLS[gCOLUMN_TYPE] },
            { "data": gUSER_COLS[gCOLUMN_DRINK] },
            { "data": gUSER_COLS[gCOLUMN_PRICE] },
            { "data": gUSER_COLS[gCOLUMN_NAME] },
            { "data": gUSER_COLS[gCOLUMN_PHONE] },
            { "data": gUSER_COLS[gCOLUMN_STATUS] },
            { "data": gUSER_COLS[gCOLUMN_ACTION] }
        ],
        // Ghi đè nội dung của cột action, chuyển thành button chi tiết
        "columnDefs": [
            {
                targets: gCOLUMN_ACTION,
                className: "w-25 text-center",
                defaultContent: "<button class='btn btn-primary btn-update'><i class='fa-solid fa-pencil'></i> Sửa</button> <button class='btn btn-danger btn-delete'><i class='fa-solid fa-trash-can'></i> Xóa</button>"
            }]
    });

    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    onPageLoading();
    getDrinkListAjaxClick();
    getPizzaSizeList(gPizzaSize);
    getPizzaTypeCreate(gPizzaType);
    modifySelector();
    $("#btn-add-order").on('click', function () {
        onBtnClickOpenModal();
    });
    $("#create-order-confirm").on('click', function () {
        onBtnClickCreateOrder();
    });
    $("#inp-combo").on('change', function () {
        insertOrderData()
    });
    $("#inp-voucherid").on('input', function () {
        onFunctionChangeDiscount();
    });
    $(document).on("click", ".btn-delete", function () {
        // lấy order id và id của order
        getOrderIdAndId(this);
        // hiện modal
        $("#delete-order-modal").modal("show")
    });
    $(document).on("click", ".btn-update", function () {
        // lấy order id và id của order
        getOrderIdAndId(this);
        // lấy data hiện lên modal
        callApiGetDataOrder();
        // hiện modal
        $("#update-order-modal").modal("show")
    });
    $("#btn-modal-delete").click(function () {
        onBtnClickDelete();
    });
    $("#btn-confirm").click(function () {
        onBtnClickConfirmOrder();
    });
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    // infoFunction sẽ là function các nút cùng gọi
    // hàm chạy khi trang được load
    function onPageLoading() {
        // lấy data từ server
        $.ajax({
            // url: "/orders",
            url: "/devcamp-pizza365/orders",
            type: "GET",
            dataType: 'json',
            success: function (responseObject) {
                gAllOrderData = responseObject.data;
                // console.log(gAllOrderData);
                loadDataToTable(gAllOrderData);
            },
            error: function (error) {
                console.assert(error.responseText);
            }
        });
    }
    function onBtnClickCreateOrder() {
        var vObjectRequest = {
            fullName: "",
            email: "",
            phone: "",
            address: "",
            kichCo: "",
            duongKinh: "",
            suon: "",
            salad: "",
            loaiPizza: "",
            maVoucher: "",
            giamGia: "",
            thanhTien: "",
            maNuocUong: "",
            soLuongNuoc: "",
            loiNhan: "",
            trangThai: "opened"
        }
        readOrderDate(vObjectRequest);
        var vkiemtra = validateOrder(vObjectRequest);
        if (vkiemtra) {
            callApiServerCreate(vObjectRequest);
            loadDataToTable(gAllOrderData);
        }
    }
    //function delete order
    function onBtnClickDelete() {
        // call api xóa order
        callApiDeleteOrder();
        // cập nhật bảng
        loadDataToTable(gAllOrderData);
        // ẩn modal 
        $("#delete-order-modal").modal("hide");
    }
    // hàm khi ấn nút confirm update
    function onBtnClickConfirmOrder() {
        var vObjectRequest = {
            trangThai: "" //3 trang thai open, confirmed, cancel
        }
        // thu thập thông tin order update trạng thái
        getDataOrderByModal(vObjectRequest);
        // gọi api confirm order
        callApiUpdateOrder(vObjectRequest);
        // cập nhật bảng
        loadDataToTable(gAllOrderData);
        // ẩn modal
        $("#detail-order-modal").modal("hide");
    }
    // hàm create order
    function onBtnClickOpenModal() {
        $("#create-order").modal('show');
    }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình */
    // load data to table
    // call api tạo đơn hàng
    function callApiServerCreate(paramObjectRequest) {
        $.ajax({
            // url: "/devcamp-pizza365/orders?email=" + paramObjectRequest.email,
            url: "/devcamp-pizza365/orders" ,
            type: "POST",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(paramObjectRequest),
            success: function (responseJson) {
                console.log(responseJson);
                swal("Pizza 365 Thông báo", "Tạo đơn hàng mới thành công", "success", {
                    buttons: {
                        catch: {
                            text: "Trở về!",
                            value: "catch",
                        },
                    },
                })
                    .then((value) => {
                        switch (value) {
                            case "catch":
                                location.reload();
                                break;
                        }
                    });
            },
            error: function (ajaxContext) {
                alert(ajaxContext.responseText);
            }
        });
    }
    // call api confirm order
    function callApiUpdateOrder(paramTrangThai) {
        $.ajax({
            url: "http://42.115.221.44:8080/devcamp-pizza365/orders" + "/" + gId,
            type: "PUT",
            async: false,
            contentType: "application/json",
            data: JSON.stringify(paramTrangThai),
            success: function (responseJson) {
                swal("Pizza 365 Thông báo", "Sửa đơn hàng thành công", "success", {
                    buttons: {
                        catch: {
                            text: "Trở về!",
                            value: "catch",
                        },
                    },
                })
                    .then((value) => {
                        switch (value) {
                            case "catch":
                                location.reload();
                                break;
                        }
                    });
            },
            error: function (ajaxContext) {
                alert(ajaxContext.responseText);
            }
        })
    }
    // load data to table
    function loadDataToTable(paramResponseObject) {
        console.log(paramResponseObject)
        //Xóa toàn bộ dữ liệu đang có của bảng
        gOrderTable.clear();
        //Cập nhật data cho bảng 
        gOrderTable.rows.add(paramResponseObject);
        //Cập nhật lại giao diện hiển thị bảng
        gOrderTable.draw();
    }
    // lấy giá trị ô input
    function readOrderDate(paramObjectRequest) {

        paramObjectRequest.kichCo = $("#inp-combo option:selected").val();
        paramObjectRequest.maNuocUong = $("#inp-drink option:selected").val();
        paramObjectRequest.maVoucher = $("#inp-voucherid").val();
        paramObjectRequest.loaiPizza = $("#inp-pizza option:selected").val();
        paramObjectRequest.fullName = $("#inp-fullname").val();
        paramObjectRequest.email = $("#inp-email").val();
        paramObjectRequest.phone = $("#inp-mobile").val();
        paramObjectRequest.address = $("#inp-address").val();
        paramObjectRequest.loiNhan = $("#inp-message").val();
        paramObjectRequest.duongKinh = $("#inp-duong-kinh").val();
        paramObjectRequest.suon = $("#inp-suong-nuong").val();
        paramObjectRequest.salad = $("#inp-Salad").val();
        paramObjectRequest.thanhTien = $("#prices-inp").val();
        paramObjectRequest.soLuongNuoc = $("#number-select").val();
    }
    // gọi api lấy nước uống
    function getDrinkListAjaxClick() {
        "use strict";
        $.ajax({
            // url: "/drink",
            url: "/devcamp-pizza365/drink",

            type: "GET",
            dataType: "json",
            success: function (responseJson) {
                // load drink vào select
                console.log(responseJson)
                loadDrinkIntoSelection(responseJson.data);
            },
            error: function (ajaxContext) {
                alert(ajaxContext.responseText);
            }
        });
    }
    // function load drink to modal
    function loadDrinkIntoSelection(paramDrink) {
        $("#inp-drink").append($("<option>", {
            text: "---chưa chọn nước uống---",
            value: 0
        }));
        $.each(paramDrink, function (index, element) {
            $("#inp-drink").append($("<option>", {
                text: element.tenNuocUong,
                value: element.maNuocUong
            }));
        })
    }
    // hàm kiểm tra thông tin trước khi tạo order
    function validateOrder(paramOrderConfirm) {
        if (paramOrderConfirm.kichCo === "0") {
            swal("Pizza 365 thông báo: ", "Bạn chưa chọn combo!", "error");
            return false;
        };
        if (paramOrderConfirm.duongKinh == "") {
            swal("Pizza 365 thông báo: ", "Bạn chưa chọn đường kính!", "error");
            return false;
        };
        if (paramOrderConfirm.suon == "") {
            swal("Pizza 365 thông báo: ", "Bạn chưa chọn sườn!", "error");
            return false;
        };
        if (paramOrderConfirm.idLoaiNuocUong === "0") {
            swal("Pizza 365 thông báo: ", "Bạn chưa chọn nước uống!", "error");
            return false;
        };
        if (paramOrderConfirm.soLuongNuoc == "") {
            swal("Pizza 365 thông báo: ", "Bạn chưa chọn số lượng nước!", "error");
            return false;
        };
        if (paramOrderConfirm.loaiPizza === "0") {
            swal("Pizza 365 thông báo: ", "Bạn chưa chọn loại pizza!", "error");
            return false;
        };
        if (paramOrderConfirm.salad == "") {
            swal("Pizza 365 thông báo: ", "Bạn chưa chọn salad!", "error");
            return false;
        };
        if (paramOrderConfirm.thanhTien == "") {
            swal("Pizza 365 thông báo: ", "Bạn chưa nhập số tiền!", "error");
            return false;
        };
        if (paramOrderConfirm.hoTen == "") {
            swal("Pizza 365 thông báo: ", "Bạn chưa nhập họ tên!", "error");
            return false;
        };
        if (!emailFilter(paramOrderConfirm.email)) {
            swal("Pizza 365 thông báo: ", "Bạn email nhập sai định dạng!", "error");
            return false
        };
        if (paramOrderConfirm.soDienThoai == "") {
            swal("Pizza 365 thông báo: ", "Bạn chưa nhập số điện thoại!", "error");
            return false;
        };
        if (paramOrderConfirm.diaChi == "") {
            swal("Pizza 365 thông báo: ", "Bạn chưa nhập địa chỉ!", "error");
            return false;
        };
        return true
    }
    // function tạo select pizza
    function getPizzaSizeList(paramPizzaType) {
        "use strict";
        $("#inp-combo").append($("<option>", {
            text: "---chưa chọn loại pizza---",
            value: 0
        }));
        $(paramPizzaType).each(function (index, element) {
            $("#inp-combo").append($("<option>", {
                text: element,
                value: element.toLowerCase()
            }));
        });
    }
    //function get pizza type select
    function getPizzaTypeCreate(paramPizzaType) {
        "use strict";
        $("#inp-pizza").append($("<option>", {
            text: "---chưa chọn loại pizza---",
            value: 0
        }));
        $(paramPizzaType).each(function (index, element) {
            $("#inp-pizza").append($("<option>", {
                text: element,
                value: element.toLowerCase()
            }));
        });
    }
    // lấy order id và id của order
    function getOrderIdAndId(paramOrder) {
        var curentRow = $(paramOrder).closest("tr")
        var curentData = gOrderTable.row(curentRow).data();
        gId = curentData._id;
        console.log(gId);
    }
    function modifySelector() {
        $('#order-select').select2({
            theme: 'bootstrap4'
        });
        $('#pizza-select').select2({
            theme: 'bootstrap4'
        });
        $('#inp-pizza').select2({
            theme: 'bootstrap4'
        });
        $('#inp-status').select2({
            theme: 'bootstrap4'
        });
        $('#inp-drink').select2({
            theme: 'bootstrap4'
        });
        $('#inp-combo').select2({
            theme: 'bootstrap4'
        });
    }
    // call api xóa order
    function callApiDeleteOrder() {
        $.ajax({
            url: "/devcamp-pizza365/orders/" + gId,
            async: false,
            contentType: "application/json",
            type: "DELETE",
            success: function (responseJson) {
                swal("Pizza 365 Thông báo", "Xóa đơn hàng thành công", "success", {
                    buttons: {
                        catch: {
                            text: "Trở về!",
                            value: "catch",
                        },
                    },
                })
                    .then((value) => {
                        switch (value) {
                            case "catch":
                                location.reload();
                                break;
                        }
                    });
            },
            error: function (ajaxContext) {
                alert(ajaxContext.responseText);
            }
        })
    }
    // call api lấy thông tin order
    function callApiGetDataOrder() {
        $.ajax({
            // url: "/orders" + "/" + gId,
            url: "/devcamp-pizza365/orders" + "/" + gId,

            type: "GET",
            success: function (responseJson) {
                console.log(responseJson)
                gOrder = responseJson;
                // hiển thị thông tin lên modal
                showDataOrderDetailToModal(responseJson);
            },
            error: function (ajaxContext) {
                console.log(ajaxContext.responseText);
            }
        })
    }
    // hiển thị thông tin lên modal
    function showDataOrderDetailToModal(paramOrderDetail) {
        $("#inp-dia-chi").val(paramOrderDetail.data.diaChi);
        $("#inp-duongkinh").val(paramOrderDetail.data.duongKinh);
        $("#inp-Email").val(paramOrderDetail.data.email);
        $("#inp-giamGia").val(paramOrderDetail.data.giamGia);
        $("#inp-hoTen").val(paramOrderDetail.data.hoTen);
        $("#inp-idLoaiNuocUong").val(paramOrderDetail.data.maNuocUong);
        $("#inp-idVourcher").val(paramOrderDetail.data.maVoucher);
        $("#inp-kichCo").val(paramOrderDetail.data.kichCo);
        $("#inp-loaiPizza").val(paramOrderDetail.data.loaiPizza);
        $("#inp-trangThai").val(paramOrderDetail.data.trangThai);
        $("#inp-LoiNhan").val(paramOrderDetail.data.loiNhan);
        $("#inp-ngayCapNhat").val((paramOrderDetail.data.ngayCapNhat));
        $("#inp-ngayTao").val((paramOrderDetail.data.ngayTao));
        $("#inp-orderId").val(paramOrderDetail.data.orderCode);
        $("#inp-salad").val(paramOrderDetail.data.salad);
        $("#inp-soDienThoai").val(paramOrderDetail.data.soDienThoai);
        $("#inp-soLuongNuoc").val(paramOrderDetail.data.soLuongNuoc);
        $("#inp-suon").val(paramOrderDetail.data.suon);
        $("#inp-thanhTien").val(paramOrderDetail.data.thanhTien);
    }
    // hàm lấy thông tin order dựa vào modal
    function getDataOrderByModal(paramTrangThai) {
        paramTrangThai.trangThai = $("#inp-trangThai").val();
    }
    // kiểm tra email
    function emailFilter(paramEmail) {
        return !!/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(paramEmail);
    }
    // điền thông tin order create
    function insertOrderData() {
        var vComboData = $("#inp-combo").val();
        for (var i = 0; i < gSelectedMenuStructure.length; i++) {
            if (vComboData === gSelectedMenuStructure[i].menuName) {
                $("#inp-duong-kinh").val(gSelectedMenuStructure[i].duongKinhCM);
                $("#inp-suong-nuong").val(gSelectedMenuStructure[i].suongNuong);
                $("#inp-Salad").val(gSelectedMenuStructure[i].saladGr);
                $("#number-select").val(gSelectedMenuStructure[i].drink);
                $("#prices-inp").val(gSelectedMenuStructure[i].priceVND);
                gThanhTien = gSelectedMenuStructure[i].priceVND;
            }
        }
        if (vComboData === "0") {
            $("#inp-duong-kinh").val("");
            $("#inp-suong-nuong").val("");
            $("#inp-Salad").val("");
            $("#number-select").val("");
            $("#prices-inp").val("");
        }
    }
    function onFunctionChangeDiscount() {
        readDataVoucher();
        if (gVoucherID.length >= 5) {
            callApiVoucher();
        }
    }
    function readDataVoucher() {
        gVoucherID = $("#inp-voucherid").val();
    }
    function callApiVoucher() {
        $.ajax({
            url: "/voucherCode?maVoucher=" + gVoucherID,
            type: "GET",
            dataType: "json",
            success: function (res) {
                console.log(res);
                handleDiscount(res);
            },
            error: function (error) {
                alert("Mã voucher sai vui lòng nhập lại!");
                $("#discount-inp").val("0 VND");
            }
        })
    }
    function handleDiscount(paramdiscount) {
        var vDiscount = paramdiscount.data.phanTramGiamGia;
        var vGiamGia = vDiscount;
        $("#discount-inp").val(vGiamGia + "%");
    }
});