// khởi tạo bộ thư viện
const express = require('express');
// import controller
const { createNewOrder,getDrinkByModel, getAllOrder, getOrderById, updateOrderById, deleteOrderById } = require('../controller/orderController');
//khởi tạo router
const router = express.Router();

// create new order
router.post('/devcamp-pizza365/orders', createNewOrder)
// get drink by drink model
router.post('/devcamp-pizza365/drinks', getDrinkByModel)
// get all orders
// router.get("/orders", getAllOrder);
router.get("/devcamp-pizza365/orders", getAllOrder);

// get order by id
// router.get("/orders/:orderId", getOrderById);
router.get("/devcamp-pizza365/orders/:orderId", getOrderById);

//update order by id
router.put("/orders/:orderId", updateOrderById);
//delete order by id
// router.delete("/orders/:orderId", deleteOrderById);
router.delete("/devcamp-pizza365/orders/:orderId", deleteOrderById);

// export dữ liệu thành 1 module
module.exports = router