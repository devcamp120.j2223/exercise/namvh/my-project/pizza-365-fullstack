// khởi tạo bộ thư viện
const express = require('express');

const { createVoucher, getAllVoucher,getVoucherByCode, getVoucherById, updateVoucherById, deleteVoucherById } = require('../controller/voucherController')

//khởi tạo router
const router = express.Router();

router.get("/voucher", getAllVoucher)


router.post("/voucher", createVoucher)

router.get("/voucher/:voucherId", getVoucherById)

router.get("/voucherCode", getVoucherByCode)
// router.get("/devcamp-pizza365/voucherCode", getVoucherByCode)

router.put("/voucher/:voucherId", updateVoucherById)

router.delete("/voucher/:voucherId", deleteVoucherById)

// export dữ liệu thành 1 module
module.exports = router;