// khởi tạo bộ thư viện
const express = require('express');

// import drink controller
const { createDrink, getAllDrink, getDrinkById, updateDrinkById, deleteDrinkById } = require('../controller/drinkController')

//khởi tạo router
const router = express.Router();

// router.get("/drink", getAllDrink)
router.get("/devcamp-pizza365/drink", getAllDrink)


router.post("/drink", createDrink)

router.get("/drink/:drinkId", getDrinkById)

router.put("/drink/:drinkId", updateDrinkById)

router.delete("/drink/:drinkId", deleteDrinkById)

// export dữ liệu thành 1 module
module.exports = router;