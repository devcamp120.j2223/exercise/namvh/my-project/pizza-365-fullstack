// câu lệnh này tương tự câu lệnh import express
const express = require("express");

// import mongoose
const mongoose = require('mongoose')
// import router
const drinkRouter = require('./app/router/drinkRouter')
const voucherRouter = require('./app/router/voucherRouter')
const userRouter = require('./app/router/userRouter')
const orderRouter = require('./app/router/orderRouter')
// import thư viện path
const path = require("path")

// khởi tạo app express
const app = express();

// khai báo middleware đọc được json
app.use(express.json());

// khai báo middleware đọc dữ liệu utf-8
app.use(express.urlencoded({
    extended: true
}))

app.use(express.static(__dirname + `/views`))

// khai báo cổng của project
const port = 8000;

mongoose.connect("mongodb://localhost:27017/CRUD_PIZZA365_1", (err) => {
    if (err) {
        throw err;
    }
    console.log("Connect mongoDB successfully")
})

// khai báo API dạng get "/" sẽ chạy vào đây
// call back function là một tham số của hàm khác và nó sẽ được thực thi ngay sau khi hàm kia được gọi
app.get("/", (request, response) => {
    console.log(__dirname);
    response.sendFile(path.join(__dirname + "/views/43.html"))
})
app.get("/detail", (request, response) => {
    console.log(__dirname);
    response.sendFile(path.join(__dirname + "/views/Detail.html"))
})

// chạy router
app.use("/", drinkRouter)
app.use("/", voucherRouter)
app.use("/", userRouter)
app.use("/", orderRouter)
// chạy app express
app.listen(port, () => {
    console.log("app listening on port (ứng dụng đang chạy trên cổng)" + port)
})
